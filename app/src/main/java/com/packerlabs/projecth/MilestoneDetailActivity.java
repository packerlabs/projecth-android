package com.packerlabs.projecth;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.packerlabs.projecth.adapters.MilestoneAdapter;
import com.packerlabs.projecth.adapters.StepsAdapter;
import com.packerlabs.projecth.interfaces.OnSelectStep;
import com.packerlabs.projecth.model.MilestoneDetail;
import com.packerlabs.projecth.model.MilestoneStep;
import com.packerlabs.projecth.utility.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by rzkdws on 02/09/2017.
 */

public class MilestoneDetailActivity extends AppCompatActivity implements OnSelectStep{

    MilestoneDetail detail;

    Gson gson_helper;

    @BindView(R.id.milestone_detail_title) TextView milestone_detail_title;

    @BindView(R.id.milestone_detail_description) TextView milestone_detail_description;

    @BindView(R.id.step_list) RecyclerView step_list;
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.toolbar_title) TextView mToolbarTitle;
    StepsAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.milestone_detail);

        ButterKnife.bind(this);

        gson_helper = new Gson();

        adapter = new StepsAdapter(this);

        step_list.setAdapter(adapter);
        step_list.setLayoutManager(new LinearLayoutManager(this));

        Bundle parameter = getIntent().getExtras();

        if(parameter != null){
            detail = gson_helper.fromJson(parameter.getString(Constants.label_milestone_data), MilestoneDetail.class);

            setMilestoneDetail(detail);
        }
    }

    public void setMilestoneDetail(MilestoneDetail data){
        milestone_detail_title.setVisibility(View.VISIBLE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            milestone_detail_description.setText(Html.fromHtml(data.getDescription(),Html.FROM_HTML_MODE_LEGACY));
        } else {
            milestone_detail_description.setText(Html.fromHtml(data.getDescription()));
        }

        mToolbarTitle.setText(data.getDisplayTitle());
        milestone_detail_title.setText(data.getDisplayTitle());

        adapter.setSteps(data.getSteps());
        adapter.notifyDataSetChanged();
    }

    @OnClick(R.id.view_milestone_steps)
    public void viewMileStoneStep(){
        startStepDetailActivity(null);
    }

    @Override
    public void showStep(MilestoneStep detail) {
        startStepDetailActivity(detail);
    }

    public void startStepDetailActivity(MilestoneStep step){
        Intent show_step_detail = new Intent(this, StepDetailActivity.class);
        String step_list = gson_helper.toJson(detail.getSteps());
        show_step_detail.putExtra(Constants.label_steps_data, step_list);
        if(step != null)
        {
            show_step_detail.putExtra(Constants.label_selected_step, step.get_id());
        }

        startActivity(show_step_detail);
    }
}
