package com.packerlabs.projecth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.packerlabs.projecth.model.response.RegistrationResponse;
import com.packerlabs.projecth.model.response.SuccessResponse;
import com.packerlabs.projecth.model.User;
import com.packerlabs.projecth.model.UserDetail;
import com.packerlabs.projecth.utility.Constants;
import com.packerlabs.projecth.utility.SharedPreferenceHelper;
import com.packerlabs.projecth.utility.WebAPI;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rzkdws on 30/08/2017.
 */

public class RegistrationActivity extends AppCompatActivity{

    @BindView(R.id.register_full_name) EditText register_full_name;
    @BindView(R.id.register_email) EditText register_email;
    @BindView(R.id.register_password) EditText register_password;
    @BindView(R.id.register_phone) EditText register_phone;
    @BindView(R.id.register_contact_preference) EditText register_contact_preference;
    @BindView(R.id.register_allow_text) EditText register_allow_text;
    @BindView(R.id.register_education) EditText register_education;
    @BindView(R.id.register_employer) EditText register_employer;
    @BindView(R.id.register_annual_salary) EditText register_annual_salary;
    @BindView(R.id.register_banker) EditText register_banker;
    @BindView(R.id.register_type_of_loan) EditText register_type_of_loan;
    @BindView(R.id.register_organization_id) EditText register_organization_id;

    SharedPreferenceHelper preference_helper;

    Gson gson_helper;

    Retrofit retrofit;

    WebAPI api_access;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        preference_helper = new SharedPreferenceHelper(this);
        gson_helper = new Gson();

        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Create an instance of our GitHub API interface.
        api_access = retrofit.create(WebAPI.class);

        loadOrganization();
    }

    public void loadOrganization(){
//        Call<SuccessResponse> load_organization_call = api_access.getOrganizations(registration_detail);
//
//        load_organization_call.enqueue(new Callback<SuccessResponse>() {
//            @Override
//            public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
//                if (response.isSuccessful()) {
//                    // tasks available
//                    SuccessResponse result = response.body();
////                        Log.d("Succeed", result.getData().toString());
//
//                    RegistrationResponse registration_result = gson_helper.fromJson(result.getData().toString(), RegistrationResponse.class);
//
//                } else {
//                    // error response, no access to resource?
//                    showToastMessage("Registration Failed, Try again later", Toast.LENGTH_LONG);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<SuccessResponse> call, Throwable t) {
//                // something went completely south (like no internet connection)
//                showToastMessage("Registration Error : " + t.getMessage(), Toast.LENGTH_LONG);
//            }
//        });
    }

    @OnClick(R.id.submit_registration)
    public void submitRegistration(){
        final String user_full_name = getEditTextInput(register_full_name);
        final String user_email = getEditTextInput(register_email);
        String user_password = getEditTextInput(register_password);
        final String user_phone = getEditTextInput(register_phone);
        String user_contact_preference = getEditTextInput(register_contact_preference);
        String user_allow_text = getEditTextInput(register_allow_text);
        String user_education = getEditTextInput(register_education);
        String user_employer = getEditTextInput(register_employer);
        String user_annual_salary = getEditTextInput(register_annual_salary);
        String user_banker = getEditTextInput(register_banker);
        String user_type_of_loan = getEditTextInput(register_type_of_loan);
        final String user_organization_id = getEditTextInput(register_organization_id);

        if(validateRegistrationInput()){


            UserDetail registration_detail = new UserDetail();
            registration_detail.setFullName(user_full_name);
            registration_detail.setEmail(user_email);
            registration_detail.setPhone(user_phone);
            registration_detail.setPassword(user_password);
            registration_detail.setPreferenceOfContact(user_contact_preference);
            registration_detail.setAllowsText(true);
            registration_detail.setEducation(user_education);
            registration_detail.setEmployer(user_employer);
            registration_detail.setAnnualSalary(Double.parseDouble(user_annual_salary));
            registration_detail.setBanker(user_banker);
            registration_detail.setTypeOfLoan(user_type_of_loan);
            registration_detail.setOrganizationID(user_organization_id);

            // Create a call instance for looking up Retrofit contributors.
            Call<RegistrationResponse> registration_call = api_access.register(registration_detail);

            registration_call.enqueue(new Callback<RegistrationResponse>() {
                @Override
                public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                    if (response.isSuccessful()) {
                        // tasks available
                        RegistrationResponse result = response.body();
//                        Log.d("Succeed", result.getData().toString());

                        User user_profile = result.getData();
                        user_profile.setPhone(user_phone);
                        user_profile.setEmail(user_email);
                        user_profile.setFullName(user_full_name);
                        user_profile.setOrganization("");
                        user_profile.setOrganizationId(user_organization_id);

                        preference_helper.rememberUserProfile(user_profile);
//
                        showHomeActivity();
                    } else {
                        // error response, no access to resource?
                        showToastMessage("Registration Failed, Try again later", Toast.LENGTH_LONG);
                    }
                }

                @Override
                public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                    // something went completely south (like no internet connection)
                    showToastMessage("Registration Error : " + t.getMessage(), Toast.LENGTH_LONG);
                }
            });
        }
    }

    public void showHomeActivity(){
        Intent home_screen = new Intent(this, HomeActivity.class);
        startActivity(home_screen);
    }

    public boolean validateRegistrationInput(){
        return true;
    }

    public String getEditTextInput(EditText form){
        return form.getText().toString();
    }

    public void showToastMessage(String message, int duration){
        Toast.makeText(this, message, duration).show();
    }
}
