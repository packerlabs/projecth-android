package com.packerlabs.projecth;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.octicons_typeface_library.Octicons;
import com.packerlabs.projecth.adapters.MilestoneAdapter;
import com.packerlabs.projecth.interfaces.OnViewMilestoneDetail;
import com.packerlabs.projecth.model.MilestoneDetail;
import com.packerlabs.projecth.model.response.ErrorResponse;
import com.packerlabs.projecth.model.response.MilestoneResponse;
import com.packerlabs.projecth.model.User;
import com.packerlabs.projecth.model.response.UserDetailResponse;
import com.packerlabs.projecth.utility.Constants;
import com.packerlabs.projecth.utility.GenericUtility;
import com.packerlabs.projecth.utility.SharedPreferenceHelper;
import com.packerlabs.projecth.utility.WebAPI;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rzkdws on 30/08/2017.
 */

public class HomeActivity extends AppCompatActivity{

    SharedPreferenceHelper preference_helper;

    @BindView(R.id.content_view) FrameLayout content_view;
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.toolbar_title) TextView toolbarTitle;

    final int INDEX_FIRST = 0;
    final int INDEX_MILESTONE = 1;
    final int INDEX_MY_PROGRESS = 2;
    final int INDEX_SIGN_OUT = 3;
    final int INDEX_ABOUT = 4;
    final int INDEX_CONTACT_AGENT = 5;
    final int INDEX_REQUEST_SUPPORT = 6;

    User current_user;

    MilestonesFragment milestone_fragment;
    UserProgressFragment user_progress_fragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        preference_helper = new SharedPreferenceHelper(this);

        current_user = preference_helper.getUserProfile();

        milestone_fragment = new MilestonesFragment();

        user_progress_fragment = new UserProgressFragment();

        createMaterialDrawer();

        showMilestones();
    }


    void createMaterialDrawer(){
//if you want to update the items at a later time it is recommended to keep it in a variable

        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withIdentifier(INDEX_MILESTONE).withIcon(Octicons.Icon.oct_milestone).withName("MILESTONES");
        PrimaryDrawerItem item2 = new PrimaryDrawerItem().withIdentifier(INDEX_MY_PROGRESS).withIcon(Octicons.Icon.oct_checklist).withName("MY PROGRESS");
        PrimaryDrawerItem item3 = new PrimaryDrawerItem().withIdentifier(INDEX_SIGN_OUT).withIcon(FontAwesome.Icon.faw_sign_out).withName("SIGN OUT");
        SecondaryDrawerItem item4 = new SecondaryDrawerItem().withIdentifier(INDEX_ABOUT).withIcon(FontAwesome.Icon.faw_info).withName("ABOUT PROJECT H");
        SecondaryDrawerItem item5 = new SecondaryDrawerItem().withIdentifier(INDEX_CONTACT_AGENT).withIcon(FontAwesome.Icon.faw_phone).withName("CONTACT YOUR AGENT");
        SecondaryDrawerItem item6 = new SecondaryDrawerItem().withIdentifier(INDEX_REQUEST_SUPPORT).withIcon(FontAwesome.Icon.faw_envelope).withName("REQUEST SUPPORT");

        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.material_drawer_badge)
                .addProfiles(
                        new ProfileDrawerItem().withName(current_user.getUserId()).withEmail(current_user.getEmail())
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();

        toolbarTitle.setText(R.string.app_name);
//create the drawer and remember the `Drawer` result object
        final Drawer result = new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(headerResult)
                .withTranslucentStatusBar(false)
                .withActionBarDrawerToggle(true)
                .withToolbar(mToolbar)
                .addDrawerItems(
                        item1, item2,
                        item3,
                        new DividerDrawerItem(),
                        item4,item5,item6
                ).build();

         result.setOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
            @Override
            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                // do something with the clicked item :D
                openDrawerSection(position, result);
                return true;
            }
        });

    }

    public void openDrawerSection(int position, Drawer drawer){
        drawer.closeDrawer();
        switch (position){
            case INDEX_MILESTONE:
            {
                showMilestones();
            }
            break;
            case INDEX_MY_PROGRESS:
            {
                showMyProgress();
            }
            break;
            case INDEX_SIGN_OUT:
            {
                processLogout();
            }
            break;
            case INDEX_ABOUT:
            {

            }
            break;
            case INDEX_CONTACT_AGENT:
            {

            }
            break;
            case INDEX_REQUEST_SUPPORT:
            {

            }
            break;
        }
        drawer.setSelection(INDEX_FIRST);
    }

    public void showMilestones(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_view, milestone_fragment);
        transaction.commit();
    }

    public void showMyProgress(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_view, user_progress_fragment);
        transaction.commit();
    }

    public void processLogout(){
        current_user.setEmail("");

        preference_helper.rememberUserProfile(current_user);

        Intent login_screen = new Intent(this, LoginActivity.class);
        startActivity(login_screen);
    }

}
