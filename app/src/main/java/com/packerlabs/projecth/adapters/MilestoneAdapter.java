package com.packerlabs.projecth.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.packerlabs.projecth.R;
import com.packerlabs.projecth.holders.MileStoneItem;
import com.packerlabs.projecth.interfaces.OnViewMilestoneDetail;
import com.packerlabs.projecth.model.MilestoneDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rzkdws on 31/08/2017.
 */

public class MilestoneAdapter extends RecyclerView.Adapter<MileStoneItem>{

    List<MilestoneDetail> milestones;

    OnViewMilestoneDetail action;

    public MilestoneAdapter(OnViewMilestoneDetail action) {
        this.milestones = new ArrayList<>();
        this.action = action;
    }

    public List<MilestoneDetail> getMilestones() {
        return milestones;
    }

    public void setMilestones(List<MilestoneDetail> milestones) {
        this.milestones = milestones;
    }

    @Override
    public MileStoneItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.milestone_item, parent, false);
        return new MileStoneItem(layout, this.action);
    }

    @Override
    public void onBindViewHolder(MileStoneItem holder, int position) {
        MilestoneDetail milestone = milestones.get(position);
        holder.setData(milestone);
    }

    @Override
    public int getItemCount() {
        return milestones.size();
    }
}
