package com.packerlabs.projecth.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.packerlabs.projecth.R;
import com.packerlabs.projecth.holders.StepHolder;
import com.packerlabs.projecth.interfaces.OnSelectStep;
import com.packerlabs.projecth.model.MilestoneStep;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rzkdws on 02/09/2017.
 */

public class StepsAdapter extends RecyclerView.Adapter<StepHolder>{

    List<MilestoneStep> steps;

    OnSelectStep action;

    public StepsAdapter(OnSelectStep action) {
        steps = new ArrayList<>();
        this.action = action;
    }

    public void setSteps(List<MilestoneStep> steps) {
        this.steps = steps;
    }

    @Override
    public StepHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.milestone_step, parent, false);
        return new StepHolder(layout, this.action);
    }

    @Override
    public void onBindViewHolder(StepHolder holder, int position) {
        MilestoneStep step = steps.get(position);
        holder.setDetail(step);
    }

    @Override
    public int getItemCount() {
        return steps.size();
    }
}
