package com.packerlabs.projecth;

import android.content.Intent;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.packerlabs.projecth.model.LoginCredential;
import com.packerlabs.projecth.model.ResetPaswordParameter;
import com.packerlabs.projecth.model.response.ForgotPasswordResponse;
import com.packerlabs.projecth.model.response.LoginResponse;
import com.packerlabs.projecth.model.User;
import com.packerlabs.projecth.model.response.SuccessResponse;
import com.packerlabs.projecth.utility.Constants;
import com.packerlabs.projecth.utility.GenericUtility;
import com.packerlabs.projecth.utility.SharedPreferenceHelper;
import com.packerlabs.projecth.utility.WebAPI;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.email) EditText email;
    @BindView(R.id.password) EditText password;
    @BindView(R.id.email_input) TextInputLayout emailInput;
    @BindView(R.id.password_input) TextInputLayout passwordInput;

    @BindView(R.id.input_login_section) LinearLayout input_login_section;
    @BindView(R.id.reset_password_section) LinearLayout reset_password_section;

    @BindView(R.id.request_email) EditText request_email;

    Gson gson_helper;

    SharedPreferenceHelper preference_helper;

    Retrofit retrofit;

    WebAPI api_access;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        gson_helper = new Gson();

        preference_helper = new SharedPreferenceHelper(this);

        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Create an instance of our GitHub API interface.
        api_access = retrofit.create(WebAPI.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            getSupportActionBar().setElevation(0);
       //     getSupportActionBar().setTitle(R.string.welcome);
        }

        if(userProfileExist()){
            showHomeActivity();
        }
    }

    @OnClick(R.id.register)
    public void showRegistrationScreen(){
        Intent registration = new Intent(this, RegistrationActivity.class);
        startActivity(registration);
    }

    @OnClick(R.id.submit_login)
    public void login(){

        String user_email = emailInput.getEditText().getText().toString();
        String user_password = passwordInput.getEditText().getText().toString();

        if(validateLoginInput(user_email, user_password)){

            // Create a call instance for looking up Retrofit contributors.
            Call<LoginResponse> login_call = api_access.login(new LoginCredential(user_email, user_password));

            login_call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    if (response.isSuccessful()) {
                        // tasks available
                        LoginResponse result = response.body();
                        Log.d("Succeed", result.getData().toString());
                        Log.d("Succeed", response.body().toString());

                        User user_profile = result.getData();

                        preference_helper.rememberUserProfile(user_profile);

                        showHomeActivity();
                    } else {
                        // error response, no access to resource?
                        GenericUtility.showToastMessage(LoginActivity.this, "Failed to Log in, Try again later", Toast.LENGTH_LONG);
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    // something went completely south (like no internet connection)
                    GenericUtility.showToastMessage(LoginActivity.this, "Login Error : " + t.getMessage(), Toast.LENGTH_LONG);
                }
            });
        }
    }

    @OnClick(R.id.reset_password_link)
    public void showInputResetPassword(){
        input_login_section.setVisibility(View.GONE);
        reset_password_section.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.submit_reset_request)
    public void submitResetPassword(){
        String user_email = request_email.getText().toString();

        ResetPaswordParameter parameter = new ResetPaswordParameter(user_email);

        Call<ForgotPasswordResponse> reset_password_call = api_access.requestResetPassword(parameter);

        reset_password_call.enqueue(new Callback<ForgotPasswordResponse>() {
            @Override
            public void onResponse(Call<ForgotPasswordResponse> call, Response<ForgotPasswordResponse> response) {
                if (response.isSuccessful()) {
                    // tasks available
                    ForgotPasswordResponse result = response.body();
                    if(result.getStatus().toLowerCase().equals(Constants.response_status_succeed))
                    {
                        GenericUtility.showToastMessage(LoginActivity.this, result.getData(), Toast.LENGTH_LONG);
                        showInputLogin();
                    }
                } else {
                    // error response, no access to resource?
                    GenericUtility.showToastMessage(LoginActivity.this, "Failed to Log in, Try again later", Toast.LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordResponse> call, Throwable t) {
                // something went completely south (like no internet connection)
                GenericUtility.showToastMessage(LoginActivity.this, "Login Error : " + t.getMessage(), Toast.LENGTH_LONG);
            }
        });
    }

    public void showInputLogin(){
        input_login_section.setVisibility(View.VISIBLE);
        reset_password_section.setVisibility(View.GONE);
    }

    public void showHomeActivity(){
        Intent home_screen = new Intent(this, HomeActivity.class);
        startActivity(home_screen);
    }

    public boolean validateLoginInput(String user_email,String user_password){
        return true;
    }

    public boolean userProfileExist(){
        User profile = preference_helper.getUserProfile();

        return profile == null? false : profile.getEmail().length() > 0;
    }
}
