package com.packerlabs.projecth.holders;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.packerlabs.projecth.R;
import com.packerlabs.projecth.interfaces.OnSelectStep;
import com.packerlabs.projecth.model.MilestoneStep;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by rzkdws on 02/09/2017.
 */

public class StepHolder extends RecyclerView.ViewHolder{

    MilestoneStep detail;

    OnSelectStep action;

    @BindView(R.id.step_marker) CheckBox step_marker;

    @BindView(R.id.step_label) TextView step_label;

    public StepHolder(View itemView, OnSelectStep action) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.action = action;
    }

    public void setDetail(MilestoneStep detail){
        this.detail = detail;
        step_label.setText(detail.getDisplayTitle());
        step_marker.setChecked(detail.isCompleted());
        Log.d("Step", detail.isCompleted()+"");
    }

    @OnClick(R.id.step_label)
    public void showStepDetail(){
        action.showStep(detail);
    }
}
