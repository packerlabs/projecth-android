package com.packerlabs.projecth.holders;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.packerlabs.projecth.R;
import com.packerlabs.projecth.interfaces.OnViewMilestoneDetail;
import com.packerlabs.projecth.model.MilestoneDetail;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by rzkdws on 31/08/2017.
 */

public class MileStoneItem extends RecyclerView.ViewHolder implements View.OnClickListener{

    @BindView(R.id.milestone_title) TextView milestone_title;
    @BindView(R.id.milestone_description) TextView milestone_description;

    OnViewMilestoneDetail action;

    MilestoneDetail data;

    public MileStoneItem(View itemView, OnViewMilestoneDetail action) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        this.action = action;

        itemView.setOnClickListener(this);
    }

    public void setData(MilestoneDetail data) {
        this.data = data;
        milestone_title.setText(data.getDisplayTitle());

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            milestone_description.setText(Html.fromHtml(data.getDescription(),Html.FROM_HTML_MODE_LEGACY));
        } else {
            milestone_description.setText(Html.fromHtml(data.getDescription()));
        }
    }

    public void viewMoreMilestoneDetail() {
        action.viewDetail(this.data);
    }

    @Override
    public void onClick(View view) {
        viewMoreMilestoneDetail();
    }
}
