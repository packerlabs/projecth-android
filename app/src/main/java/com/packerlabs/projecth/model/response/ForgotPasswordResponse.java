package com.packerlabs.projecth.model.response;

import com.packerlabs.projecth.model.User;

/**
 * Created by rzkdws on 08/09/2017.
 */

public class ForgotPasswordResponse {
    String status;
    String data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
