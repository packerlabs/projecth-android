package com.packerlabs.projecth.model.response;

import com.packerlabs.projecth.model.UserMilestone;

/**
 * Created by rzkdws on 02/09/2017.
 */

public class UserMilestoneResponse {
    String status;

    UserMilestone data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserMilestone getData() {
        return data;
    }

    public void setData(UserMilestone data) {
        this.data = data;
    }
}
