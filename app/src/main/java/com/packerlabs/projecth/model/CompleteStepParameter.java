package com.packerlabs.projecth.model;

/**
 * Created by rzkdws on 06/09/2017.
 */

public class CompleteStepParameter {
    String stepId;

    public CompleteStepParameter(String stepId) {
        this.stepId = stepId;
    }

    public String getStepId() {
        return stepId;
    }

    public void setStepId(String stepId) {
        this.stepId = stepId;
    }
}
