package com.packerlabs.projecth.model;

/**
 * Created by rzkdws on 02/09/2017.
 */

public class MilestoneInput {
    String displayTitle;
	String description;
	String status;
	int order;

    public MilestoneInput(String displayTitle, String description, String status, int order) {
        this.displayTitle = displayTitle;
        this.description = description;
        this.status = status;
        this.order = order;
    }

    public String getDisplayTitle() {
        return displayTitle;
    }

    public void setDisplayTitle(String displayTitle) {
        this.displayTitle = displayTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
