package com.packerlabs.projecth.model.response;

import com.packerlabs.projecth.model.MilestoneDetail;

import java.util.List;

/**
 * Created by rzkdws on 31/08/2017.
 */

public class MilestoneResponse {
    String status;

    List<MilestoneDetail> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<MilestoneDetail> getData() {
        return data;
    }

    public void setData(List<MilestoneDetail> data) {
        this.data = data;
    }
}
