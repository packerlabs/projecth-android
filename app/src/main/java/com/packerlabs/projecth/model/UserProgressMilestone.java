package com.packerlabs.projecth.model;

import java.util.List;

/**
 * Created by rzkdws on 02/09/2017.
 */

public class UserProgressMilestone {

    String milestoneID; //": "59a2d0e24cb0e1511e415309",

    String displayTitle; //": "Milestone Test",

    String description; //": "<p>This is the first MIlestone!</p><p><br/></p>",

    List<MilestoneStep> stepsRequired; //": [

    List<String> stepsPending; //": [],

    List<String> stepsCompleted; //": []

    public UserProgressMilestone(String milestoneID, String displayTitle, String description, List<MilestoneStep> stepsRequired, List<String> stepsPending, List<String> stepsCompleted) {
        this.milestoneID = milestoneID;
        this.displayTitle = displayTitle;
        this.description = description;
        this.stepsRequired = stepsRequired;
        this.stepsPending = stepsPending;
        this.stepsCompleted = stepsCompleted;
    }

    public String getMilestoneID() {
        return milestoneID;
    }

    public void setMilestoneID(String milestoneID) {
        this.milestoneID = milestoneID;
    }

    public String getDisplayTitle() {
        return displayTitle;
    }

    public void setDisplayTitle(String displayTitle) {
        this.displayTitle = displayTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MilestoneStep> getStepsRequired() {
        return stepsRequired;
    }

    public void setStepsRequired(List<MilestoneStep> stepsRequired) {
        this.stepsRequired = stepsRequired;
    }

    public List<String> getStepsPending() {
        return stepsPending;
    }

    public void setStepsPending(List<String> stepsPending) {
        this.stepsPending = stepsPending;
    }

    public List<String> getStepsCompleted() {
        return stepsCompleted;
    }

    public void setStepsCompleted(List<String> stepsCompleted) {
        this.stepsCompleted = stepsCompleted;
    }

    public MilestoneDetail getMilestoneDetail(){
        MilestoneDetail data = new MilestoneDetail(getMilestoneID(), getDisplayTitle(), getDescription(), 0, 0, "", "", "", "", false, getStepsRequired());

        return data;
    }
}
