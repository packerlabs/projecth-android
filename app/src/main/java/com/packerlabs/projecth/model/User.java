package com.packerlabs.projecth.model;

/**
 * Created by rzkdws on 30/08/2017.
 */

public class User {
    String token; //": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1OWEyZDE3MjRjYjBlMTUxMWU0MTUzMGUiLCJlbWFpbCI6InRlYW1AcGFja2VybGFicy5jb20iLCJvcmdhbml6YXRpb25JZCI6IjU5OWI0MGE3ZTFhMzNkNTBlNGI2ZDRlZCIsImlhdCI6MTUwNDA3Nzc3N30.vdsvowYkoR_uu2jBvqBs49Is83JNdHpGtlaIs_BGwnw",
    String userId; //": "59a2d1724cb0e1511e41530e",
    String phone; //": "6182077553",
    String email; //": "team@packerlabs.com",
    String fullName; //": "Test Account",
    String organization; //": "test",
    String organizationId; //": "599b40a7e1a33d50e4b6d4ed"



    public User(String token, String userId, String phone, String email, String fullName, String organization, String organizationId) {
        this.token = token;
        this.userId = userId;
        this.phone = phone;
        this.email = email;
        this.fullName = fullName;
        this.organization = organization;
        this.organizationId = organizationId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }
}
