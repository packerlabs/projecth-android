package com.packerlabs.projecth.model.response;

import com.packerlabs.projecth.model.MilestoneStep;

import org.json.JSONObject;

/**
 * Created by rzkdws on 02/09/2017.
 */

public class ViewStepResponse {
    String status;

    MilestoneStep data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MilestoneStep getData() {
        return data;
    }

    public void setData(MilestoneStep data) {
        this.data = data;
    }
}
