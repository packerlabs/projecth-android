package com.packerlabs.projecth.model;

/**
 * Created by rzkdws on 31/08/2017.
 */

public class MilestoneStep {
    String _id; //": "59a2d10e4cb0e1511e41530b",
    String displayTitle; //": "Step One"

//    "_id": "59a2d10e4cb0e1511e41530b",
//            "displayTitle": "Step One",
    String description; //": "<blockquote><p>Here is a test step of ours.</p><p>​</p></blockquote>",
    String milestoneID; //": "59a2d0e24cb0e1511e415309",
    String status; //": "1",
    int order; //": 1,
    String createdOn; //": "2017-08-27T14:02:54.309Z",
    String lastUpdatedOn; //": "2017-08-27T14:02:54.309Z",
    String __v; //": 0,
    boolean isArchived; //": false,
    boolean containsMedia; //": true,
    boolean requireReview; //": false,
    boolean completed;//": false,
    boolean canUserComplete; //": true,
    Object media; //": null

    public MilestoneStep(String _id, String displayTitle) {
        this._id = _id;
        this.displayTitle = displayTitle;
    }

    public MilestoneStep(String _id, String displayTitle, String description, String milestoneID, String status, int order, String createdOn, String lastUpdatedOn, String __v, boolean isArchived, boolean containsMedia, boolean requireReview, boolean completed, boolean canUserComplete, Object media) {
        this._id = _id;
        this.displayTitle = displayTitle;
        this.description = description;
        this.milestoneID = milestoneID;
        this.status = status;
        this.order = order;
        this.createdOn = createdOn;
        this.lastUpdatedOn = lastUpdatedOn;
        this.__v = __v;
        this.isArchived = isArchived;
        this.containsMedia = containsMedia;
        this.requireReview = requireReview;
        this.completed = completed;
        this.canUserComplete = canUserComplete;
        this.media = media;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getDisplayTitle() {
        return displayTitle;
    }

    public void setDisplayTitle(String displayTitle) {
        this.displayTitle = displayTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMilestoneID() {
        return milestoneID;
    }

    public void setMilestoneID(String milestoneID) {
        this.milestoneID = milestoneID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(String lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String get__v() {
        return __v;
    }

    public void set__v(String __v) {
        this.__v = __v;
    }

    public boolean isArchived() {
        return isArchived;
    }

    public void setArchived(boolean archived) {
        isArchived = archived;
    }

    public boolean isContainsMedia() {
        return containsMedia;
    }

    public void setContainsMedia(boolean containsMedia) {
        this.containsMedia = containsMedia;
    }

    public boolean isRequireReview() {
        return requireReview;
    }

    public void setRequireReview(boolean requireReview) {
        this.requireReview = requireReview;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public boolean isCanUserComplete() {
        return canUserComplete;
    }

    public void setCanUserComplete(boolean canUserComplete) {
        this.canUserComplete = canUserComplete;
    }

    public Object getMedia() {
        return media;
    }

    public void setMedia(Object media) {
        this.media = media;
    }
}
