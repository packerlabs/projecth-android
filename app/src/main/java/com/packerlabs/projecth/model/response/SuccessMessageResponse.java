package com.packerlabs.projecth.model.response;

/**
 * Created by rzkdws on 08/09/2017.
 */

public class SuccessMessageResponse {
    String status;

    String message;

    public SuccessMessageResponse(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
