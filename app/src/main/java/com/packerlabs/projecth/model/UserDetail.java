package com.packerlabs.projecth.model;

/**
 * Created by rzkdws on 30/08/2017.
 */

public class UserDetail {
    String _id; //": "59a2d1724cb0e1511e41530e",
    String fullName; //": "Test Account",
    String email; //": "team@packerlabs.com",
    String password; //": "$2a$10$HmIZGbWrd2QZZcC8QkGRt.1LuAoXpcNye58SW5dHVAWp39uQ4hWem",
    String preferenceOfContact; //": "phone",
    boolean allowsText;//    "": "true",
    String organizationID;//    "": "599b40a7e1a33d50e4b6d4ed",
    String createdOn;//    "": "2017-08-27T14:04:34.924Z",
    String __v;//    "": 0,
    boolean isArchived;//    "": false,
    //    "stepsPending": [],
//    "stepProgress": [],
//    "mileStoneProgress": [],
    String typeOfLoan;//    "": "Personal",
    String banker;//    "banker": "Personal",
    double annualSalary;//    "": "1000000",
    String employer;//    "": "Self",
    String birthDate;//    "": "2017-08-27T14:03:48.273Z",
    String education;//    "": "Bachelor's",
    String phone;//    "": "6182077553",
    String address;//    "": "1000 S Brehm Ln, Carbondale, IL 62901"

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPreferenceOfContact() {
        return preferenceOfContact;
    }

    public void setPreferenceOfContact(String preferenceOfContact) {
        this.preferenceOfContact = preferenceOfContact;
    }

    public boolean isAllowsText() {
        return allowsText;
    }

    public void setAllowsText(boolean allowsText) {
        this.allowsText = allowsText;
    }

    public String getOrganizationID() {
        return organizationID;
    }

    public void setOrganizationID(String organizationID) {
        this.organizationID = organizationID;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String get__v() {
        return __v;
    }

    public void set__v(String __v) {
        this.__v = __v;
    }

    public boolean isArchived() {
        return isArchived;
    }

    public void setArchived(boolean archived) {
        isArchived = archived;
    }

    public String getTypeOfLoan() {
        return typeOfLoan;
    }

    public void setTypeOfLoan(String typeOfLoan) {
        this.typeOfLoan = typeOfLoan;
    }

    public String getBanker() {
        return banker;
    }

    public void setBanker(String banker) {
        this.banker = banker;
    }

    public double getAnnualSalary() {
        return annualSalary;
    }

    public void setAnnualSalary(double annualSalary) {
        this.annualSalary = annualSalary;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
