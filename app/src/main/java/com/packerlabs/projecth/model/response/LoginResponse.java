package com.packerlabs.projecth.model.response;

import com.packerlabs.projecth.model.User;

import org.json.JSONObject;

/**
 * Created by rzkdws on 31/08/2017.
 */

public class LoginResponse {
    String status;

    User data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public User getData() {
        return data;
    }

    public void setData(User data) {
        this.data = data;
    }
}
