package com.packerlabs.projecth.model.response;

/**
 * Created by rzkdws on 14/09/2017.
 */

public class ErrorResponse {
    String status;

    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
