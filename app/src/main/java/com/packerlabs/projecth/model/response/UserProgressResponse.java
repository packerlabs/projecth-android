package com.packerlabs.projecth.model.response;

import com.packerlabs.projecth.model.UserProgressMilestone;

import java.util.List;

/**
 * Created by rzkdws on 02/09/2017.
 */

public class UserProgressResponse {
    String status;

    List<UserProgressMilestone> data;

    public UserProgressResponse(String status, List<UserProgressMilestone> data) {
        this.status = status;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<UserProgressMilestone> getData() {
        return data;
    }

    public void setData(List<UserProgressMilestone> data) {
        this.data = data;
    }
}
