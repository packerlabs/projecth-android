package com.packerlabs.projecth.model.response;

import org.json.JSONObject;

/**
 * Created by rzkdws on 30/08/2017.
 */

public class SuccessResponse {
    String status;

    JSONObject data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }
}
