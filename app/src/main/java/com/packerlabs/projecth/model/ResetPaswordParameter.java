package com.packerlabs.projecth.model;

/**
 * Created by rzkdws on 06/09/2017.
 */

public class ResetPaswordParameter {

    String email;

    public ResetPaswordParameter(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
