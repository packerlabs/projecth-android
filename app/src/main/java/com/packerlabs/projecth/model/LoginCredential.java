package com.packerlabs.projecth.model;

/**
 * Created by rzkdws on 30/08/2017.
 */

public class LoginCredential {
    String email;

    String password;

    public LoginCredential(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
