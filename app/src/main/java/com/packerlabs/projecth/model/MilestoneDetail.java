package com.packerlabs.projecth.model;

import java.util.List;

/**
 * Created by rzkdws on 31/08/2017.
 */

public class MilestoneDetail {
    String _id; //": "59a2d0e24cb0e1511e415309",
    String displayTitle; //": "Milestone Test",
    String description; //": "<p>This is the first MIlestone!</p><p><br/></p>",
    int status; //": "1",
    int order; //": 1,
    String createdOn; //": "2017-08-27T14:02:10.049Z",
    String lastUpdatedOn;//": "2017-08-27T14:02:10.049Z",
    String organizationID; //": "599b40a7e1a33d50e4b6d4ed",
    String __v; //": 0,
    boolean isArchived; //": false,
    List<MilestoneStep> steps;

    public MilestoneDetail(String _id, String displayTitle, String description, int status, int order, String createdOn, String lastUpdatedOn, String organizationID, String __v, boolean isArchived, List<MilestoneStep> steps) {
        this._id = _id;
        this.displayTitle = displayTitle;
        this.description = description;
        this.status = status;
        this.order = order;
        this.createdOn = createdOn;
        this.lastUpdatedOn = lastUpdatedOn;
        this.organizationID = organizationID;
        this.__v = __v;
        this.isArchived = isArchived;
        this.steps = steps;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getDisplayTitle() {
        return displayTitle;
    }

    public void setDisplayTitle(String displayTitle) {
        this.displayTitle = displayTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(String lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getOrganizationID() {
        return organizationID;
    }

    public void setOrganizationID(String organizationID) {
        this.organizationID = organizationID;
    }

    public String get__v() {
        return __v;
    }

    public void set__v(String __v) {
        this.__v = __v;
    }

    public boolean isArchived() {
        return isArchived;
    }

    public void setArchived(boolean archived) {
        isArchived = archived;
    }

    public List<MilestoneStep> getSteps() {
        return steps;
    }

    public void setSteps(List<MilestoneStep> steps) {
        this.steps = steps;
    }
}
