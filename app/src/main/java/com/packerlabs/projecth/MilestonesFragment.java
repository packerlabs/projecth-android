package com.packerlabs.projecth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.packerlabs.projecth.adapters.MilestoneAdapter;
import com.packerlabs.projecth.interfaces.OnViewMilestoneDetail;
import com.packerlabs.projecth.model.MilestoneDetail;
import com.packerlabs.projecth.model.User;
import com.packerlabs.projecth.model.response.ErrorResponse;
import com.packerlabs.projecth.model.response.MilestoneResponse;
import com.packerlabs.projecth.model.response.UserProgressResponse;
import com.packerlabs.projecth.utility.Constants;
import com.packerlabs.projecth.utility.GenericUtility;
import com.packerlabs.projecth.utility.SharedPreferenceHelper;
import com.packerlabs.projecth.utility.WebAPI;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rzkdws on 14/09/2017.
 */

public class MilestonesFragment extends Fragment implements OnViewMilestoneDetail {

    @BindView(R.id.milestone_list) RecyclerView milestone_list;

    MilestoneAdapter adapter;

    final int MILESTONE_COLUMN_COUNT = 2;

    Gson gson_helper;

    Retrofit retrofit;

    User current_user;

    SharedPreferenceHelper preference_helper;

    public MilestonesFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new MilestoneAdapter(this);

        gson_helper = new Gson();

        preference_helper = new SharedPreferenceHelper(getContext());

        current_user = preference_helper.getUserProfile();

        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.milestone_fragment, container, false);
        ButterKnife.bind(this, view);

        milestone_list.setLayoutManager(new GridLayoutManager(container.getContext(), MILESTONE_COLUMN_COUNT));

        milestone_list.setAdapter(adapter);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadMilestones();
    }


    @Override
    public void viewDetail(MilestoneDetail detail) {
        Intent view_detail = new Intent(getContext(), MilestoneDetailActivity.class);
        view_detail.putExtra(Constants.label_milestone_data, gson_helper.toJson(detail));
        startActivity(view_detail);
    }

    public void loadMilestones(){

        // Create an instance of our GitHub API interface.
        WebAPI api_access = retrofit.create(WebAPI.class);

        // Create a call instance for looking up Retrofit contributors.
        Call<MilestoneResponse> load_milestones = api_access.getMilestones(current_user.getToken());

        load_milestones.enqueue(new Callback<MilestoneResponse>() {
            @Override
            public void onResponse(Call<MilestoneResponse> call, Response<MilestoneResponse> response) {
                if (response.isSuccessful()) {
                    // tasks available
                    MilestoneResponse result = response.body();

                    Log.d("Succeed", result.getData().toString());

                    showMilestoneList(result.getData());

                } else {
                    // error response, no access to resource?
                    ErrorResponse error = GenericUtility.parseError(response, retrofit);
                    showErrorMessage(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<MilestoneResponse> call, Throwable t) {
                // something went completely south (like no internet connection)
                showErrorMessage("Load Milestone Error : " + t.getMessage());
            }
        });
    }

//    public void loadMyProgress(){
//
//
//        // Create an instance of our GitHub API interface.
//        WebAPI api_access = retrofit.create(WebAPI.class);
//
//        // Create a call instance for looking up Retrofit contributors.
//        Call<UserProgressResponse> load_milestones = api_access.getUserProgress(current_user.getToken(), current_user.getUserId());
//
//        load_milestones.enqueue(new Callback<UserProgressResponse>() {
//            @Override
//            public void onResponse(Call<UserProgressResponse> call, Response<UserProgressResponse> response) {
//                if (response.isSuccessful()) {
//                    // tasks available
//                    UserProgressResponse result = response.body();
//
//                    Log.d("Succeed", result.getData().toString());
//
//                    showMilestoneList(result.getData().getMilestones());
//
//                } else {
//                    // error response, no access to resource?
//                    ErrorResponse error = GenericUtility.parseError(response, retrofit);
//                    showErrorMessage(error.getMessage());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<UserProgressResponse> call, Throwable t) {
//                // something went completely south (like no internet connection)
//                GenericUtility.showToastMessage(this, "Login Error : " + t.getMessage(), Toast.LENGTH_LONG);
//            }
//        });
//    }

    public void showErrorMessage(String message){
        GenericUtility.showToastMessage(getContext(), message, Toast.LENGTH_LONG);
    }

    public void showMilestoneList(List<MilestoneDetail> milestones){
        adapter.setMilestones(milestones);
        adapter.notifyDataSetChanged();
    }
}
