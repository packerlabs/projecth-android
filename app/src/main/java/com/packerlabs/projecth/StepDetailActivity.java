package com.packerlabs.projecth;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.packerlabs.projecth.model.CompleteStepParameter;
import com.packerlabs.projecth.model.MilestoneStep;
import com.packerlabs.projecth.model.User;
import com.packerlabs.projecth.model.response.SuccessMessageResponse;
import com.packerlabs.projecth.model.response.ViewStepResponse;
import com.packerlabs.projecth.utility.Constants;
import com.packerlabs.projecth.utility.GenericUtility;
import com.packerlabs.projecth.utility.SharedPreferenceHelper;
import com.packerlabs.projecth.utility.WebAPI;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rzkdws on 02/09/2017.
 */

public class StepDetailActivity extends AppCompatActivity{

    @BindView(R.id.step_detail_title) TextView step_detail_title;
    @BindView(R.id.step_detail_description) TextView step_detail_description;
    @BindView(R.id.step_detail_notification_status) TextView step_detail_notification_status;

    @BindView(R.id.set_completed_step) Button set_completed_step;
    @BindView(R.id.show_previous_step) Button show_previous_step;
    @BindView(R.id.show_next_step) Button show_next_step;
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.toolbar_title) TextView mToolbarTitle;

    List<MilestoneStep> steps;

    Gson gson_helper;

    int step_index;

    final int FIRST_INDEX = 0;
    int last_index;

    SharedPreferenceHelper preference_helper;

    User current_user;

    MilestoneStep current_step_detail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.step_detail);

        ButterKnife.bind(this);

        gson_helper = new Gson();

        preference_helper = new SharedPreferenceHelper(this);

        Bundle milestone_step_data = getIntent().getExtras();

        step_index = 0;

        current_user = preference_helper.getUserProfile();

        if(milestone_step_data != null){
            Type type_step = new TypeToken<ArrayList<MilestoneStep>>(){}.getType();
            String steps_json = milestone_step_data.getString(Constants.label_steps_data, "");
            steps = gson_helper.fromJson(steps_json, type_step);
            last_index = steps.size() - 1;

            String step_id = milestone_step_data.getString(Constants.label_selected_step, "");

            if(step_id.isEmpty())
            {
                step_id = steps.get(step_index).get_id();
            }
            else{

                for (MilestoneStep data: steps){
                    if(data.get_id().equals(step_id))
                    {
                        break;
                    }

                    step_index++;
                }
            }
            loadStepDetail(current_user.getToken(), step_id);
        }
    }

    public void loadStepDetail(String token, String step_id){

        updateNavigationButton(step_index);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Create an instance of our GitHub API interface.
        WebAPI api_access = retrofit.create(WebAPI.class);

        // Create a call instance for looking up Retrofit contributors.
        Call<ViewStepResponse> login_call = api_access.getStepDetail(token, step_id);

        login_call.enqueue(new Callback<ViewStepResponse>() {
            @Override
            public void onResponse(Call<ViewStepResponse> call, Response<ViewStepResponse> response) {
                if (response.isSuccessful()) {
                    // tasks available
                    ViewStepResponse result = response.body();
                    Log.d("Succeed", result.getData().toString());

                    current_step_detail = result.getData();

                    setStepDetail(current_step_detail);
                } else {
                    // error response, no access to resource?
                    GenericUtility.showToastMessage(StepDetailActivity.this, "Failed to Log in, Try again later", Toast.LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<ViewStepResponse> call, Throwable t) {
                // something went completely south (like no internet connection)
                GenericUtility.showToastMessage(StepDetailActivity.this, "Login Error : " + t.getMessage(), Toast.LENGTH_LONG);
            }
        });
    }

    public void setStepDetail(MilestoneStep step_detail){
        step_detail_title.setText(step_detail.getDisplayTitle());
        mToolbarTitle.setText(step_detail.getDisplayTitle());

        if(step_detail.isCompleted()){
            set_completed_step.setEnabled(false);
            set_completed_step.setText(R.string.step_completed);
        }else{
            set_completed_step.setEnabled(true);
            set_completed_step.setText(R.string.mark_as_completed);
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            step_detail_description.setText(Html.fromHtml(step_detail.getDescription(),Html.FROM_HTML_MODE_LEGACY));
        } else {
            step_detail_description.setText(Html.fromHtml(step_detail.getDescription()));
        }
    }

    public void updateNavigationButton(int index){
        show_previous_step.setEnabled(index > FIRST_INDEX);
        show_next_step.setEnabled(index < last_index);
    }

    @OnClick(R.id.contact_agent)
    public void contactAgent(){

    }

    @OnClick(R.id.set_completed_step)
    public void setCompletedStep(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Create an instance of our GitHub API interface.
        WebAPI api_access = retrofit.create(WebAPI.class);

        CompleteStepParameter parameter = new CompleteStepParameter(current_step_detail.get_id());

        // Create a call instance for looking up Retrofit contributors.
        Call<SuccessMessageResponse> complete_step_call = api_access.completeStep(current_user.getToken(), parameter);

        complete_step_call.enqueue(new Callback<SuccessMessageResponse>() {
            @Override
            public void onResponse(Call<SuccessMessageResponse> call, Response<SuccessMessageResponse> response) {
                if (response.isSuccessful()) {
                    // tasks available
                    SuccessMessageResponse result = response.body();
                    if(result.getStatus().toLowerCase().equals(Constants.response_status_succeed)){
                        GenericUtility.showToastMessage(StepDetailActivity.this, result.getMessage(), Toast.LENGTH_LONG);
                    }

                } else {
                    // error response, no access to resource?
                    GenericUtility.showToastMessage(StepDetailActivity.this, "Failed to Mark Step as Completed, Try again later", Toast.LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<SuccessMessageResponse> call, Throwable t) {
                // something went completely south (like no internet connection)
                GenericUtility.showToastMessage(StepDetailActivity.this, "Mark Step as Completed Error : " + t.getMessage(), Toast.LENGTH_LONG);
            }
        });
    }

    @OnClick(R.id.show_previous_step)
    public void showPreviousStep(){
        if(step_index > FIRST_INDEX){
            step_index--;
            loadStepDetail(current_user.getToken(), steps.get(step_index).get_id());
        }
    }

    @OnClick(R.id.show_next_step)
    public void showNextStep(){
        if(step_index < last_index){
            step_index++;
            loadStepDetail(current_user.getToken(), steps.get(step_index).get_id());
        }
    }

}
