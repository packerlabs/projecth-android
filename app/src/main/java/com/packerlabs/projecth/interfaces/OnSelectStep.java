package com.packerlabs.projecth.interfaces;

import com.packerlabs.projecth.model.MilestoneStep;

/**
 * Created by rzkdws on 16/09/2017.
 */

public interface OnSelectStep {
    public void showStep(MilestoneStep detail);
}
