package com.packerlabs.projecth.interfaces;

import com.packerlabs.projecth.model.MilestoneDetail;

/**
 * Created by rzkdws on 02/09/2017.
 */

public interface OnViewMilestoneDetail {
    public void viewDetail(MilestoneDetail detail);
}
