package com.packerlabs.projecth.utility;

/**
 * Created by rzkdws on 30/08/2017.
 */

public class Constants {
    public static final String API_URL = "http://52.23.156.36:3000";

    public static final String label_milestone_data = "milestones";
    public static final String label_steps_data = "milestone_steps";
    public static final String response_status_succeed = "success";
    public static final String response_status_failed = "failure";
    public static final String label_selected_step = "selected_step";
}
