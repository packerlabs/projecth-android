package com.packerlabs.projecth.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.packerlabs.projecth.model.User;

/**
 * Created by rzkdws on 30/08/2017.
 */

public class SharedPreferenceHelper {

    final String key_user_token = "token";
    final String key_user_id = "user_id";
    final String key_email = "email";
    final String key_phone = "phone";
    final String key_full_name = "full_name";
    final String key_organization = "organization";
    final String key_organization_id = "organization_id";

    SharedPreferences preference;

    public SharedPreferenceHelper(Context context) {
        preference = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void rememberUserProfile(User user){
        SharedPreferences.Editor editor = preference.edit();
        editor.putString(key_user_token, user.getToken());
        editor.putString(key_user_id, user.getUserId());
        editor.putString(key_email, user.getEmail());
        editor.putString(key_phone, user.getPhone());
        editor.putString(key_full_name, user.getToken());
        editor.putString(key_organization, user.getOrganization());
        editor.putString(key_organization_id, user.getOrganizationId());
        editor.commit();
    }

    public User getUserProfile(){

        String email = preference.getString(key_email, "");
        String full_name = preference.getString(key_full_name, "");
        String organization_id = preference.getString(key_organization_id, "");
        String organization = preference.getString(key_organization, "");
        String phone = preference.getString(key_phone, "");
        String token = preference.getString(key_user_token, "");
        String user_id = preference.getString(key_user_id, "");

        User profile = new User(token, user_id, phone, email, full_name, organization, organization_id);

        return profile;
    }
}
