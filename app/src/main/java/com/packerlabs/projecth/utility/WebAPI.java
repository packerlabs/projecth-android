package com.packerlabs.projecth.utility;

import com.packerlabs.projecth.model.CompleteStepParameter;
import com.packerlabs.projecth.model.LoginCredential;
import com.packerlabs.projecth.model.MilestoneStep;
import com.packerlabs.projecth.model.ResetPaswordParameter;
import com.packerlabs.projecth.model.response.ForgotPasswordResponse;
import com.packerlabs.projecth.model.response.LoginResponse;
import com.packerlabs.projecth.model.response.MilestoneResponse;
import com.packerlabs.projecth.model.response.RegistrationResponse;
import com.packerlabs.projecth.model.response.SuccessMessageResponse;
import com.packerlabs.projecth.model.response.SuccessResponse;
import com.packerlabs.projecth.model.UserDetail;
import com.packerlabs.projecth.model.response.UserDetailResponse;
import com.packerlabs.projecth.model.response.UserMilestoneResponse;
import com.packerlabs.projecth.model.response.UserProgressResponse;
import com.packerlabs.projecth.model.response.ViewStepResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by rzkdws on 30/08/2017.
 */

public interface WebAPI {
    @POST("/api/user/login")
    Call<LoginResponse> login(@Body LoginCredential credential);

    @GET("/api/organization")
    Call<SuccessResponse> getOrganizations(@Header("Authorization") String authorization);

    @POST("/api/user")
    Call<RegistrationResponse> register(@Body UserDetail registrationInput);

    @GET("/api/milestones")
    Call<MilestoneResponse> getMilestones(@Header("Authorization") String authorization);

    @GET("/api/steps/{stepsId}")
    Call<ViewStepResponse> getStepDetail(@Header("Authorization") String authorization, @Path("stepsId")String stepsId);

    @GET("api/userProgress/{userId}")
    Call<UserProgressResponse> getUserProgress(@Header("Authorization") String authorization, @Path("userId")String user_id);

    @GET("api/milestones/{milestonesId}")
    Call<UserMilestoneResponse> getMilestoneDetail(@Header("Authorization") String authorization, @Path("milestonesId")String milestonesId);

    @PUT("api/steps")
    Call<SuccessResponse> updateStep(@Header("Authorization") String authorization, @Path("stepsId")String stepsId, @Body MilestoneStep parameter);

    @POST("api/user/markStepAsCompleted")
    Call<SuccessMessageResponse> completeStep(@Header("Authorization") String authorization, @Body CompleteStepParameter parameter);

    @POST("api/user/forgotPassword")
    Call<ForgotPasswordResponse> requestResetPassword(@Body ResetPaswordParameter parameter);
}
