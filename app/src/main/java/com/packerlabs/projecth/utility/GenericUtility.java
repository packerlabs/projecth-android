package com.packerlabs.projecth.utility;

import android.content.Context;
import android.widget.Toast;

import com.packerlabs.projecth.model.response.ErrorResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by rzkdws on 02/09/2017.
 */

public class GenericUtility {

    public static void showToastMessage(Context activity, String message, int duration){
        Toast.makeText(activity, message, duration).show();
    }

    public static ErrorResponse parseError(Response<?> response, Retrofit instance) {
        Converter<ResponseBody, ErrorResponse> converter = instance.responseBodyConverter(ErrorResponse.class, new Annotation[0]);

        ErrorResponse error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ErrorResponse();
        }

        return error;
    }
}
